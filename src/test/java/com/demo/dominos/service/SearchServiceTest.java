package com.demo.dominos.service;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.demo.dominos.model.Piece;
import com.demo.dominos.model.Placement;
import com.demo.dominos.model.Solutions;

public class SearchServiceTest {

	SearchService service = new SearchService();

	static Piece p(int a, int b) {
		return new Piece(a, b);
	}

	@Test
	public void testNoPieces() {
		Solutions solutions = service.search(p(1, 2), emptyList());
		assertThat(solutions.getValue()).isEqualTo(0);
		assertThat(solutions.getChains()).hasSize(1);

		List<Placement> collect = solutions.getChains().stream().flatMap(List::stream).collect(toList());
		assertThat(collect).extracting(Placement::getHead, Placement::getTail).containsExactly(tuple(1, 2));
	}

	@Test
	public void testNonFittingPiece() {
		Solutions solutions = service.search(p(1, 2), asList(p(3, 4)));
		assertThat(solutions.getValue()).isEqualTo(0);
		assertThat(solutions.getChains()).hasSize(1);

		List<Placement> collect = solutions.getChains().stream().flatMap(List::stream).collect(toList());
		assertThat(collect).extracting(Placement::getHead, Placement::getTail).containsExactly(tuple(1, 2));
	}

	@Test
	public void testOneFittingPiece() {
		Solutions solutions = service.search(p(1, 2), asList(p(2, 4)));
		assertThat(solutions.getValue()).isEqualTo(2);
		assertThat(solutions.getChains()).hasSize(1);

		List<Placement> collect = solutions.getChains().stream().flatMap(List::stream).collect(toList());
		assertThat(collect).extracting(Placement::getHead, Placement::getTail).containsExactly(tuple(1, 2),
				tuple(2, 4));

	}

	@Test
	void testTwoNonFittingPieces() {
		Solutions solutions = service.search(p(1, 6), asList(p(3, 4), p(5, 2)));
		assertThat(solutions.getValue()).isEqualTo(0);
		assertThat(solutions.getChains()).hasSize(1);

		List<Placement> collect = solutions.getChains().stream().flatMap(List::stream).collect(toList());
		assertThat(collect).extracting(Placement::getHead, Placement::getTail).containsExactly(tuple(1, 6));
	}

	@Test
	void testOneFittingAndOneNonFittingPieces() {
		Solutions solutions = service.search(p(1, 5), asList(p(3, 4), p(5, 2)));
		assertThat(solutions.getValue()).isEqualTo(5);
		assertThat(solutions.getChains()).hasSize(1);

		List<Placement> collect = solutions.getChains().stream().flatMap(List::stream).collect(toList());
		assertThat(collect).extracting(Placement::getHead, Placement::getTail).containsExactly(tuple(1, 5),
				tuple(5, 2));
	}

	@Test
	void testTwoFittingPieces() {
		Solutions solutions = service.search(p(3, 2), asList(p(1, 2), p(2, 4)));
		assertThat(solutions.getValue()).isEqualTo(2);
		assertThat(solutions.getChains()).hasSize(2);

		List<Placement> collect = solutions.getChains().stream().flatMap(List::stream).collect(toList());
		assertThat(collect).extracting(Placement::getHead, Placement::getTail).containsExactly(tuple(3, 2), tuple(2, 4),
				tuple(3, 2), tuple(2, 1));
	}

	@Test
	void testTwoFittingAndOneNonFittingPieces() {
		Solutions solutions = service.search(p(1, 6), asList(p(1, 2), p(1, 3), p(3, 4)));
		assertThat(solutions.getValue()).isEqualTo(4);
		assertThat(solutions.getChains()).hasSize(1);

		List<Placement> collect = solutions.getChains().stream().flatMap(List::stream).collect(toList());
		assertThat(collect).extracting(Placement::getHead, Placement::getTail).containsExactly(tuple(4, 3), tuple(3, 1),
				tuple(1, 6));
	}

	@Test
	void testThreeFittingPieces() {
		Solutions solutions = service.search(p(1, 6), asList(p(1, 2), p(1, 2), p(1, 2)));
		assertThat(solutions.getValue()).isEqualTo(4);
		assertThat(solutions.getChains()).hasSize(1);

		List<Placement> collect = solutions.getChains().stream().flatMap(List::stream).collect(toList());
		assertThat(collect).extracting(Placement::getHead, Placement::getTail).containsExactly(tuple(2, 1), tuple(1, 2),
				tuple(2, 1), tuple(1, 6));
	}

	@Test
	void testFivePieces() {
		Solutions solutions = service.search(p(1, 6), asList(p(1, 2), p(2, 5), p(5, 4), p(6, 3)));
		assertThat(solutions.getValue()).isEqualTo(14);
		assertThat(solutions.getChains()).hasSize(1);

		List<Placement> collect = solutions.getChains().stream().flatMap(List::stream).collect(toList());
		assertThat(collect).extracting(Placement::getHead, Placement::getTail).containsExactly(tuple(4, 5), tuple(5, 2),
				tuple(2, 1), tuple(1, 6), tuple(6, 3));
	}

	@Test
	void testEightPieces() {
		Solutions solutions = service.search(p(4, 3),
				asList(p(5, 4), p(1, 4), p(1, 6), p(4, 3), p(3, 5), p(6, 2), p(6, 5)));
		assertThat(solutions.getValue()).isEqualTo(23);
		assertThat(solutions.getChains()).hasSize(23);

		List<Placement> collect = solutions.getChains().stream().flatMap(List::stream).collect(toList());
		assertThat(collect).extracting(Placement::getHead, Placement::getTail).containsExactly( //
				tuple(5, 3), tuple(3, 4), tuple(4, 1), tuple(1, 6), tuple(6, 5), tuple(5, 4), tuple(4, 3), //
				tuple(3, 4), tuple(4, 1), tuple(1, 6), tuple(6, 5), tuple(5, 4), tuple(4, 3), tuple(3, 5), //
				tuple(2, 6), tuple(6, 5), tuple(5, 4), tuple(4, 3), tuple(3, 4), tuple(4, 1), tuple(1, 6), //
				tuple(6, 5), tuple(5, 4), tuple(4, 3), tuple(3, 4), tuple(4, 1), tuple(1, 6), tuple(6, 2), //
				tuple(5, 6), tuple(6, 1), tuple(1, 4), tuple(4, 3), tuple(3, 5), tuple(5, 4), tuple(4, 3), //
				tuple(2, 6), tuple(6, 1), tuple(1, 4), tuple(4, 3), tuple(3, 5), tuple(5, 4), tuple(4, 3), //
				tuple(3, 5), tuple(5, 4), tuple(4, 3), tuple(3, 4), tuple(4, 1), tuple(1, 6), tuple(6, 5), //
				tuple(3, 5), tuple(5, 4), tuple(4, 3), tuple(3, 4), tuple(4, 1), tuple(1, 6), tuple(6, 2), //
				tuple(5, 4), tuple(4, 3), tuple(3, 4), tuple(4, 1), tuple(1, 6), tuple(6, 5), tuple(5, 3), //
				tuple(5, 4), tuple(4, 3), tuple(3, 5), tuple(5, 6), tuple(6, 1), tuple(1, 4), tuple(4, 3), //
				tuple(5, 4), tuple(4, 1), tuple(1, 6), tuple(6, 5), tuple(5, 3), tuple(3, 4), tuple(4, 3), //
				tuple(5, 6), tuple(6, 1), tuple(1, 4), tuple(4, 5), tuple(5, 3), tuple(3, 4), tuple(4, 3), //
				tuple(2, 6), tuple(6, 1), tuple(1, 4), tuple(4, 5), tuple(5, 3), tuple(3, 4), tuple(4, 3), //
				tuple(3, 4), tuple(4, 3), tuple(3, 5), tuple(5, 6), tuple(6, 1), tuple(1, 4), tuple(4, 5), //
				tuple(3, 4), tuple(4, 3), tuple(3, 5), tuple(5, 4), tuple(4, 1), tuple(1, 6), tuple(6, 5), //
				tuple(3, 4), tuple(4, 3), tuple(3, 5), tuple(5, 4), tuple(4, 1), tuple(1, 6), tuple(6, 2), //
				tuple(5, 3), tuple(3, 4), tuple(4, 5), tuple(5, 6), tuple(6, 1), tuple(1, 4), tuple(4, 3), //
				tuple(3, 4), tuple(4, 5), tuple(5, 6), tuple(6, 1), tuple(1, 4), tuple(4, 3), tuple(3, 5), //
				tuple(3, 5), tuple(5, 6), tuple(6, 1), tuple(1, 4), tuple(4, 3), tuple(3, 4), tuple(4, 5), //
				tuple(5, 6), tuple(6, 1), tuple(1, 4), tuple(4, 3), tuple(3, 4), tuple(4, 5), tuple(5, 3), //
				tuple(2, 6), tuple(6, 1), tuple(1, 4), tuple(4, 3), tuple(3, 4), tuple(4, 5), tuple(5, 6), //
				tuple(2, 6), tuple(6, 1), tuple(1, 4), tuple(4, 3), tuple(3, 4), tuple(4, 5), tuple(5, 3), //
				tuple(6, 1), tuple(1, 4), tuple(4, 3), tuple(3, 4), tuple(4, 5), tuple(5, 6), tuple(6, 2));

	}
}
