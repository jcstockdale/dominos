package com.demo.dominos.rest;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.demo.dominos.dto.PieceDto;
import com.demo.dominos.dto.SearchRequestDto;
import com.demo.dominos.dto.SearchResponseDto;
import com.demo.dominos.model.Piece;
import com.demo.dominos.model.Placement;
import com.demo.dominos.model.Solutions;
import com.demo.dominos.service.SearchService;

@ExtendWith(MockitoExtension.class)
public class SearchControllerTest {

	@Mock
	SearchService service;

	@InjectMocks
	SearchController controller;

	@Test
	public void testGetChains() throws Exception {
		Set<List<Placement>> chains = singleton(asList(new Placement(new Piece(5, 6), true)));
		when(service.search(any(), any())).thenReturn(new Solutions(1L, chains));

		SearchRequestDto request = new SearchRequestDto(new PieceDto(1, 2), asList(new PieceDto(2, 1)));
		SearchResponseDto response = controller.getChains(request);

		verify(service).search(new Piece(1, 2), asList(new Piece(2, 1)));
		assertThat(response.getHighestValue()).isEqualTo(1L);
		assertThat(response.getSolutions()).hasSize(1);
		List<PieceDto> placements = response.getSolutions().stream().flatMap(List::stream).collect(toList());
		assertThat(placements).extracting(PieceDto::getHead, PieceDto::getTail).containsExactly(tuple(6, 5));
	}

	@Test
	public void testHandleAllExceptions() throws Exception {
		ResponseEntity<Object> responseEntity = controller.handleAllExceptions(new Exception(), null);
		assertThat(responseEntity.getStatusCode()).isEqualTo(INTERNAL_SERVER_ERROR);
		assertThat(responseEntity.getBody()).isNotNull();
	}

	@Test
	public void testFromDto() throws Exception {
		Piece fromDto = controller.fromDto(new PieceDto(1, 2));
		assertThat(fromDto).isEqualTo(new Piece(1, 2));
	}

	@Test
	public void testToDto() throws Exception {
		SearchResponseDto dto = controller.toDto(new Solutions(1L, singleton(asList(new Placement(new Piece(5, 6), true)))));
		assertThat(dto.getHighestValue()).isEqualTo(1L);
		assertThat(dto.getSolutions()).hasSize(1);
		List<PieceDto> placements = dto.getSolutions().stream().flatMap(List::stream).collect(toList());
		assertThat(placements).extracting(PieceDto::getHead, PieceDto::getTail).containsExactly(tuple(6, 5));
	}

}
