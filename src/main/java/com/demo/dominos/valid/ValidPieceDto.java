package com.demo.dominos.valid;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import com.demo.dominos.dto.PieceDto;

/**
 * Validation constraint for a PieceDto. Ensures that the head and tail are in
 * range and are not equal to each other.
 * 
 * @author stockj1
 *
 */
@Constraint(validatedBy = PieceDtoValidator.class)
@Target(TYPE)
@Retention(RUNTIME)
public @interface ValidPieceDto {
	int min();

	int max();

	String message() default "{error.piece}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}

class PieceDtoValidator implements ConstraintValidator<ValidPieceDto, PieceDto> {
	private ValidPieceDto annotation;

	@Override
	public void initialize(ValidPieceDto annotation) {
		this.annotation = annotation;
	}

	@Override
	public boolean isValid(PieceDto value, ConstraintValidatorContext context) {
		return inRange(value.getHead()) && inRange(value.getTail()) && value.getHead() != value.getTail();
	}

	private boolean inRange(int n) {
		return annotation.min() <= n && n <= annotation.max();
	}
}