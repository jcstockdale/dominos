package com.demo.dominos.service;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.springframework.stereotype.Service;

import com.demo.dominos.model.Piece;
import com.demo.dominos.model.Placement;
import com.demo.dominos.model.Solutions;

import lombok.extern.slf4j.Slf4j;

/**
 * Service class that performs the search for solutions
 * 
 * @author stockj1
 *
 */
@Slf4j
@Service
public class SearchService {

	public Solutions search(Piece start, Collection<Piece> pieces) {
		log.info("New search: {} -> {}", start, pieces);
		Map<Piece, Integer> countsByPiece = pieces.stream().collect(toMap(identity(), x -> 1, Integer::sum));
		TreeMap<Long, Set<List<Placement>>> acc = new TreeMap<>();

		// Get the best chains by placing the start piece without flipping it
		search(new Placement(start, false), countsByPiece, acc);

		Solutions solutions = new Solutions(acc.lastKey(), acc.lastEntry().getValue());

		log.info("Highest value is {}", solutions.getValue());
		log.info("There are {} solution(s):", solutions.getChains().size());
		solutions.getChains().stream().map(String::valueOf).forEach(log::info);

		return solutions;
	}

	void search(Placement start, Map<Piece, Integer> remainingPieces, NavigableMap<Long, Set<List<Placement>>> acc) {
		Deque<Placement> deque = new LinkedList<>();
		deque.add(start);
		search(deque, remainingPieces, 0, acc);
	}

	void search(Deque<Placement> placedPieces, Map<Piece, Integer> remainingPieces, long value,
			NavigableMap<Long, Set<List<Placement>>> acc) {
		// head is the value at the 'left' of the game state
		final int head = placedPieces.peekFirst().getHead();
		// tail is the value at the 'right' of the game state
		final int tail = placedPieces.peekLast().getTail();
		
		// Get only the unique fitting pieces so that identical subtrees are not explored
		
		// Pieces containing the head value can be tried on the left
		Set<Piece> uniqueHeadFittingPieces = remainingPieces.keySet().stream().filter(p -> p.has(head)).collect(toSet());

		// Pieces containing the tail value can be tried on the right
		Set<Piece> uniqueTailFittingPieces = remainingPieces.keySet().stream().filter(p -> p.has(tail)).collect(toSet());

		if (uniqueHeadFittingPieces.isEmpty() && uniqueTailFittingPieces.isEmpty()) {
			// No fitting pieces left. This is the end of the line.
			Long highestValue;
			if (acc.isEmpty() || (highestValue = acc.lastKey()) == null || value >= highestValue) {
				acc.computeIfAbsent(value, x -> new LinkedHashSet<>()).add(new ArrayList<>(placedPieces));
			}
		} else {
			// For all pieces that can attach to the head of the current game state
			for (Piece candidatePiece : uniqueHeadFittingPieces) {
				// create a placement that orientates the piece for the head
				Placement placement = candidatePiece.forHead(head);
				search(placedPieces, remainingPieces, value, acc, head, placement, Deque::addFirst, Deque::removeFirst);
			}

			// For all pieces that can attach to the tail of the current game state
			for (Piece candidatePiece : uniqueTailFittingPieces) {
				// create a placement that orientates the piece for the tail
				Placement placement = candidatePiece.forTail(tail);
				search(placedPieces, remainingPieces, value, acc, tail, placement, Deque::addLast, Deque::removeLast);
			}
		}
	}
	
	/**
	 * @param placedPieces the current game state
	 * @param remainingPieces the pieces available to play
	 * @param value the score of the current game state
	 * @param acc an accumulator for collecting results
	 * @param score the additional value added by the current placement
	 * @param placement the new piece that is being added to the game state
	 * @param addOp an operator for adding the piece to the game state
	 * @param removeOp an operator for removing the piece from the game state
	 */
	private void search(Deque<Placement> placedPieces, Map<Piece, Integer> remainingPieces, long value,
			NavigableMap<Long, Set<List<Placement>>> acc, int score, final Placement placement,
			BiConsumer<Deque<Placement>, Placement> addOp, Consumer<Deque<Placement>> removeOp) {

		// remove a piece before trying it
		dec(remainingPieces, placement.getPiece());
		// push the piece to the deque in the correct orientation
		addOp.accept(placedPieces, placement);
		// recursively search the subspace
		search(placedPieces, remainingPieces, value + score, acc);
		// pop the tried piece back off the deque
		removeOp.accept(placedPieces);
		// return the piece to the remaining pieces ready to try the next one
		inc(remainingPieces, placement.getPiece());
	}

	static <T> void inc(Map<T, Integer> map, T key) {
		// increment the value for a given key
		map.merge(key, 1, Integer::sum);
	}

	static <T> void dec(Map<T, Integer> map, T key) {
		// decrement the value for a given key, removing the mapping if the value becomes zero
		map.merge(key, 1, (a, b) -> a.equals(b) ? null : a - b);
	}
}
