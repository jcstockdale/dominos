
package com.demo.dominos.model;

import lombok.Value;

/**
 * Model class for a domino piece
 * @author stockj1
 *
 */
@Value
public class Piece {
	/**
	 * The 'left' number
	 */
	Integer head;
	
	/**
	 * The 'right' number
	 */
	Integer tail;

	/**
	 * Returns true if this piece has the specified number as its head or tail
	 */
	public boolean has(int n) {
		return n == head || n == tail;
	}
	
	public Placement forTail(int tail) {
		return new Placement(this, this.getHead() != tail);
	}
	
	public Placement forHead(int head) {
		return new Placement(this, this.getTail() != head);
	}
	
	@Override
	public String toString() {
		return "[" + head + ":" + tail + "]";
	}	
}