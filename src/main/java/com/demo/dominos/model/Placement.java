package com.demo.dominos.model;

import lombok.Value;

/**
 * Model object for a domino piece placed in a particular orientation
 * @author stockj1
 *
 */
@Value
public class Placement {

	/**
	 * The placed piece
	 */
	Piece piece;
	
	/**
	 * True means the piece was placed with its 'left' and 'right' reversed.
	 */
	boolean flipped;

	/**
	 * Returns the 'right' number in the domino's placed orientation
	 */
	public int getTail() {
		return flipped ? piece.getHead() : piece.getTail();
	}
	
	/**
	 * Returns the 'left' number in the domino's placed orientation
	 */
	public int getHead() {
		return flipped ? piece.getTail() : piece.getHead();
	}

	@Override
	public String toString() {
		return "[" + getHead() + ":" + getTail() + "]";
	}

}
