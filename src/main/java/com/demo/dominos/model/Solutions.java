package com.demo.dominos.model;

import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * A container for a set of solutions with a certian value
 *
 */
@Data
@AllArgsConstructor
public class Solutions {

	/**
	 * The value of the solutions
	 */
	private Long value;

	/**
	 * Collection containing Placements that make the solutions. Each element in the
	 * set is one solution with the given value. Each element of the solution is a
	 * domino in a particular orientation
	 */
	private Set<List<Placement>> chains;
}
