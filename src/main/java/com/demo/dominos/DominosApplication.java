package com.demo.dominos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point for the application
 */
@SpringBootApplication
public class DominosApplication {

	public static void main(String[] args) {
		SpringApplication.run(DominosApplication.class, args);
	}
}
