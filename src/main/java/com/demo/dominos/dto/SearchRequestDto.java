package com.demo.dominos.dto;

import java.util.List;

import javax.validation.Valid;

import org.springframework.lang.NonNull;

import lombok.Data;

/**
 * Dto for a search request
 * @author stockj1
 *
 */
@Data
public class SearchRequestDto {

	@NonNull
	@Valid
	private PieceDto start;

	@NonNull
	@Valid
	private List<PieceDto> pieces;

}
