package com.demo.dominos.dto;

import java.util.List;

import lombok.Value;

/**
 * Dto for an error response
 * @author stockj1
 *
 */
@Value
public class ErrorResponseDto {
	private String message;
	private List<String> details;
}
