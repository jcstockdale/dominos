package com.demo.dominos.dto;

import java.util.List;
import java.util.Set;

import lombok.Data;

/**
 * Dto for a search response
 * @author stockj1
 *
 */
@Data
public class SearchResponseDto {
	
	private Long highestValue;
	
	private Set<List<PieceDto>> solutions;
	
}
