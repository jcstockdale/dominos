package com.demo.dominos.dto;

import com.demo.dominos.valid.ValidPieceDto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Dto for a placed or unplaced domino
 * @author stockj1
 *
 */
@Data
@AllArgsConstructor
@ValidPieceDto(min =1 , max = 10)
public class PieceDto {
	private Integer head, tail;
}
