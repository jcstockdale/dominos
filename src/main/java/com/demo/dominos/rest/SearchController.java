package com.demo.dominos.rest;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.demo.dominos.dto.ErrorResponseDto;
import com.demo.dominos.dto.PieceDto;
import com.demo.dominos.dto.SearchRequestDto;
import com.demo.dominos.dto.SearchResponseDto;
import com.demo.dominos.model.Piece;
import com.demo.dominos.model.Solutions;
import com.demo.dominos.service.SearchService;

import lombok.extern.slf4j.Slf4j;

/**
 * REST controller for the search service
 * @author stockj1
 *
 */
@RestController
@Slf4j
public class SearchController {

	@Autowired
	private SearchService service;

	@PostMapping("/dominoes")
	public SearchResponseDto getChains(@RequestBody @Valid SearchRequestDto request) {

		Solutions solutions = service.search(
				fromDto(request.getStart()),
				request.getPieces().stream().map(this::fromDto).collect(toList()));
		
		return toDto(solutions);
	}
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		log.warn("Error", ex);
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponseDto error = new ErrorResponseDto("Server Error", details);
        return new ResponseEntity<>(error, INTERNAL_SERVER_ERROR);
    }
	
	Piece fromDto(PieceDto dto) {
		return new Piece(dto.getHead(), dto.getTail());
	}
	
	SearchResponseDto toDto(Solutions solutions) {
		SearchResponseDto searchResponse = new SearchResponseDto();
		searchResponse.setHighestValue(solutions.getValue());
		searchResponse.setSolutions(solutions.getChains().stream()
				.map(lp -> lp.stream()
						.map(p -> new PieceDto(p.getHead(), p.getTail())).collect(toList()))
				.collect(toSet()));
		return searchResponse;
	}
}
